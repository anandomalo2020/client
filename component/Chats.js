import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Chats extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  contactScreen =() => {
    this.props.navigation.navigate('Contacts')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> chats </Text>
      <TouchableOpacity style={styles.iconfixed} onPress={this.contactScreen}>
        <View >
              <Icon
          name="message-text-outline"
          backgroundColor="#3b5998"
          onPress={this.loginWithFacebook}
          style={styles.iconCol}
        />
        </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  iconfixed:{
    backgroundColor:'green',
    borderRadius:50,
    width:60,
    height:60,
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    right:20,
    bottom:20
  },
  iconCol:{
    fontSize:25,
    color:"#fff"
  }
});