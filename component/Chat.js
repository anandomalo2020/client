import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat'; // 0.3.0
import { StatusBar } from 'expo-status-bar';
import { YellowBox } from 'react-native';
import io from 'socket.io-client'
import _ from 'lodash';
import { AsyncStorage } from 'react-native';
import axios from '../config/api_client'
export default class Chat extends Component {
  // user= {"id":"9f53a158-e8be-414d-b8d3-c6e7989a87f2","name":"anando","mobile":"8882479313","updatedAt":"2020-08-06T11:34:35.563Z","createdAt":"2020-08-06T11:34:35.563Z"}
  // messages=[{
  //   _id: 3,
  //   text: 'Hello developer',
  //   createdAt: new Date(),
  //   user: {
  //     _id: 1,
  //     name: 'React Native',
  //     avatar: 'https://placeimg.com/140/140/any',
  //   },
  // }];
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      message: '',
      user: {},
      loadPrevMessages:true,
      userInfo:''
    };
  }

  componentDidMount() {
    this.userInfo = this.props.route.params.userInfo;
    this.getCurrentUser();
    var connectedUser = {};
    let androidStudioLocalhostUrl ="http://10.0.2.2"
    this.socket = io.connect("https://desolate-basin-74718.herokuapp.com");
    this.socket.emit("assign_socket_to_mobile", 8882479313)
    this.socket.on('connect', () => {
      console.log("user id", this.socket.id); // an alphanumeric id...
    });
    //console.log("session id",socketConnection)
    this.socket.on("chat messages", msg => {
      console.log("message recived on client")
      this.getAllMessages();
     // this.messages.push(msg)
    })

    /*Received private messages*/
    // socket.on('private_chat', function (data) {
    //   var username = data.username;
    //   var message = data.message;
    // });
      console.log('chats' , this.userInfo)
  }

  getAllMessages (){
    axios.get("allMessages").then(res => {
      let temp = [];
      let a = res.data.map((item => {
        item.sent = true;
        item.received = true;
        item.pending = true;
       // console.log("message item",item)
        item.createdAt = new Date(item.createdAt);
      }))
      // this.setState({loadPrevMessages:true})
      if (res.data.length > 0) {
        this.setState({ messages: res.data })
        let a = this.state.messages;
        this.setState({ messages: a })
        this.setState({loadPrevMessages:false})
      }
      console.log("all messages")

    }).catch(err => {

    })
  }

  getCurrentUser = async () => {
    const user = await AsyncStorage.getItem('user')
    let temp = JSON.parse(user);
    if (temp != null) {
      let tempUser = {
        _id: temp.id,
        name: temp.name
      }
      this.setState({ user: tempUser })
      console.log("current user ", this.state.user)
      this.getAllMessages();
    }
  }

  saveMessage(message) {
    axios.post("message", message).then(res => {
      console.log("message saved response")
    }).catch(err => {
      console.log("message saved err response")
    })
  }


  sendMessage(msg) {
    let textM = msg[0].text
    let tempMessages = this.state.messages;
    tempMessages.push(msg[0]);
    this.setState({ messages: tempMessages })
    console.log("send message === ", msg[0]);
    this.saveMessage(msg[0]);
    this.socket.emit("chat message", msg[0])
    this.getAllMessages();
    this.setState({ message: "" })

    // for sending private message

    //   this.socket.emit('private_chat',{
    //     to : mobile,
    //     message : message
    // });
  }

  render() {
    return (
      <View style={styles.container}>
        
        <GiftedChat
        loadEarlier={this.state.loadPrevMessages}
        isTyping={true}
          messages={this.state.messages}
          onSend={(message) => {
            this.sendMessage(message);
          }}
          user={this.state.user}
        />
        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
});

YellowBox.ignoreWarnings(['componentWillReceiveProps']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('componentWillReceiveProps') <= -1) {
    _console.warn(message);
  }
};