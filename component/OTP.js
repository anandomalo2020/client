import React, { Component } from 'react';
import { View, Text, TextInput, Button, ToastAndroid, StyleSheet, SafeAreaView } from 'react-native';
import axios from '../config/api_client'
import { AsyncStorage } from 'react-native';
import Chat from './Chat'
import MainChatScreen from  './MainChats'

export default class OTP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otpVerify: true,
      otpValue: 1234,
      name: '',
      pin1: '',
      pin2: '',
      pin3: '',
      pin4: '',
      mobile: '',
    };
  }

  componentDidMount() {
    this.setState({ mobile: this.props.route.params.mobile })
    console.log("mobile number", this.props.route.params.mobile)
    // this.props.navigation.navigate('Chat')
  }

  sendOtp = async () => {
    ToastAndroid.showWithGravity(
      "OTP resend to your Number",
      ToastAndroid.SHORT,
      ToastAndroid.CENTER
    );
  }

  verifyOtp = async () => {
    let pin1 = this.state.pin1;
    let pin2 = this.state.pin2;
    let pin3 = this.state.pin3;
    let pin4 = this.state.pin4;
    if (pin1.concat(pin2.concat(pin3).concat(pin4)) == this.state.otpValue) {
      console.log("OTP Matche", this.state.otpValue)
      this.setState({ otpVerify: false })
    } else {
      if (pin1 == "" || pin2 == "" || pin3 == "" || pin4 == "") {
        ToastAndroid.showWithGravity(
          "Please Enter OTP to your Number",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        this.setState({ otpVerify: true })
      } else {
        ToastAndroid.showWithGravity(
          "Please Enter Correct OTP ",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );
        this.setState({ otpVerify: true })
      }

    }

  }

  createUser = async () => {

  }

  submitName = async () => {
    if (this.state.name != "") {
      let user = {
        name: this.state.name,
        mobile: this.state.mobile
      }

      axios.post("mobile",user).then(res=>{
        if(res.data.length > 0){
          console.log("check user is present",res.data[0])
          AsyncStorage.setItem('isLoggedIn', 'true');
          AsyncStorage.setItem('user',JSON.stringify(res.data[0]))
          console.log("localasorage user", res.data[0])
          this.props.navigation.navigate('Chat', {
            user: res.data[0]
          })    
         
        }else{
       axios.post("newUser", user)
        .then(response => {
          if (response.data.status == 500) {
            throw new Error("Some error occured while creating Account");
            console.log("error in saving user", response.data.message);
          } else {          
              AsyncStorage.setItem('isLoggedIn', 'true');
              AsyncStorage.setItem('user',JSON.stringify(response.data))
              console.log("localasorage user", response.data)
              this.tabNavigation();
              this.props.navigation.navigate('Chat', {
                user: response.data
              })          
          }
        }).catch(function (error) {
          console.log("error1 in saving user", error);
        })
          console.log("check user is not present",res.data.length)
        }
      }).catch(err=>{

      })
      //console.log("name", userObj)

    }
  }
  

  render() {
    const { pin1, pin2, pin3, pin4 } = this.state
    return (
      <SafeAreaView>
        <View>
          {this.state.otpVerify ?
            (<View style={styles.container}>
              <Text>Enter Otp</Text>
              <View style={{ flexDirection: "row" }}>
                <TextInput autoFocus={true} textAlign={'center'} value={pin1} ref={'pin1Ref'} onChangeText={(pin1) => {
                  this.setState({ pin1: pin1 })
                  if (pin1 != "") {
                    this.refs.pin2Ref.focus()
                  }
                }
                } maxLength={1} style={{ height: 40, borderColor: 'gray', marginRight: 10, borderWidth: 1 }}></TextInput>
                <TextInput textAlign={'center'} value={pin2} ref={'pin2Ref'} maxLength={1} onChangeText={(pin2) => {
                  this.setState({ pin2: pin2 })
                  if (pin2 != "") {
                    this.refs.pin3Ref.focus()
                  }
                }} style={{ height: 40, borderColor: 'gray', marginRight: 10, borderWidth: 1 }}></TextInput>
                <TextInput textAlign={'center'} value={pin3} ref={'pin3Ref'} maxLength={1} onChangeText={(pin3) => {
                  this.setState({ pin3: pin3 })
                  if (pin3 != "") {
                    this.refs.pin4Ref.focus()
                  }
                }} style={{ height: 40, borderColor: 'gray', marginRight: 10, borderWidth: 1 }}></TextInput>
                <TextInput textAlign={'center'} value={pin4} ref={'pin4Ref'} maxLength={1} onChangeText={(pin4) => this.setState({ pin4: pin4 })} style={{ height: 40, borderColor: 'gray', marginRight: 10, borderWidth: 1 }}></TextInput>
              </View>
              <View style={{ flexDirection: "column" }}>
                <View style={{ flexDirection: 'row' }}>
                  <Button
                    onPress={this.verifyOtp}
                    title="Next"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                  /><Button
                    onPress={this.sendOtp}
                    title="Resend OTP"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                  />
                </View>

              </View>
            </View>)
            :
            (<View>
              <Text>Enter Name</Text>
              <TextInput autoFocus={true} autoFocus={true} textAlign={'center'} style={{ height: 40, width: 100, borderColor: 'gray', borderWidth: 1 }} onChangeText={(name) => this.setState({ name })}></TextInput>
              <Button onPress={this.submitName} title="Next" color="#841584" accessibilityLabel="Learn more about this purple button" />
            </View>)
          }
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
});