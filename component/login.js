import React, { Component } from 'react';
import { View, Text, Button, TextInput, ToastAndroid } from 'react-native';
import axios from '../config/api_client'
import {AsyncStorage} from 'react-native';  

class login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstOtp:'',
            otpSent:false,
            mobile:''
        };
    }

    componentDidMount(){
        // AsyncStorage.setItem('isLoggedIn', 'false'); 
       //this.props.navigation.navigate('Contacts');
        this._checkUserisLoggedIn();
    }

    _checkUserisLoggedIn = async()=>{
        let isLoggedIn = null
        isLoggedIn = await AsyncStorage.getItem('isLoggedIn'); 
        console.log("localstorage",isLoggedIn)
        if(isLoggedIn == 'true'){
            console.log("navigate chat")
            this.props.navigation.navigate('Chat');
        }
    }

   getUser = async()=>{
       axios.get('user').then(re=>{
        console.log(" test data ", re.data)
       }).catch(function (error) {
        console.log(error);
      })
   }


   mobilevalidate(mobileNumber) {
    const reg = /^[0]?[789]\d{9}$/;
    if (reg.test(mobileNumber)) {
      this.setState({
        mobile: mobileNumber,
      });
      return true;
    } else {
      return false;
      
    }
  }

   sendOtp = async()=>{
       if(this.mobilevalidate(this.state.mobile)){
        if(this.state.mobile != "" && this.state.mobile.length == 10){
            this.setState({
                otpSent:true
            })
            ToastAndroid.showWithGravity(
                "OTP sent to your Number",
                ToastAndroid.SHORT,
                ToastAndroid.CENTER
              );
              this.props.navigation.navigate('OTP',{
                  mobile:this.state.mobile
              })
           }
       }else{
        ToastAndroid.showWithGravity(
            "Please enter correct Mobile Num",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
       }
       
    
}

  

    render() {
        const  {mobile} = this.state;
        return (
            <View>
                <View>
                <Text>Mobile No :</Text>
                <TextInput autoFocus={true} maxLength={10} textAlign={'center'} style={{ height: 40, width:100, borderColor: 'gray', borderWidth: 1 }} value={mobile} onChangeText={(mobile)=>this.setState({mobile})}></TextInput>
                  <Button onPress={this.sendOtp} title="Send OTP" color="#841584" accessibilityLabel="Learn more about this purple button" />
                  </View>
            </View>
        );
    }
}

export default login;
