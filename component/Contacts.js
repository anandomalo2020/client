import React, { Component } from 'react';
import { View, Text, SafeAreaView, ScrollView, Image, StyleSheet, TouchableOpacity } from 'react-native';
import * as Contact from 'expo-contacts';
import { TextInput } from 'react-native-gesture-handler';
import axios from '../config/api_client'
import { Searchbar } from 'react-native-paper';

export default class Contacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactList: [],
      mobileNumberList:[],
      filterContactList:[],
      search:'',
      searchQuery:'',
      setSearchQuery:'',
      availableList:[]
    };
  }

  async getContactPermission() {
    const { status } = await Contact.requestPermissionsAsync();
    if (status === 'granted') {
      const contacts = await Contact.getContactsAsync({
        fields: [Contact.PHONE_NUMBERS],
        //pageSize: 12,
        pageOffset: 0,
      });
      if (contacts.total > 0) {
        this.setState({ contactList: contacts.data })
       // console.log("total contacts ",contacts.data[0].phoneNumbers[0].number)
        // console.log(
        //   'Your first contact is...',
        //   `Name: ${contacts.data[0].name}\n` +
        //   `id: ${contacts.data[0].id}\n` +
        //   `Phone numbers: ${contacts.data[0].phoneNumbers.id}\n`
        // );
      //  console.log("contacts list ", JSON.stringify(this.state.contactList))
      }
    }
  }

  filterontactList(){
    let contactTemp  = this.state.contactList;
    let filterContacts=[];
    let allMobileLis=this.state.mobileNumberList;
    contactTemp.forEach(function(item){
      if(item.phoneNumbers){
        item.phoneNumbers.filter(function(num){
          allMobileLis.forEach(function(mob){
         
            let a = num.number.toString();
            a =  a.replace(/[()-]/g,'');
            a =  a.replace(/ /g,'');
         //   console.log('test a-',  a)
            let b = mob.mobile.toString();
         //   console.log('test b-',  b)
            if(a.includes(b)){
          //    console.log("contact list",a,b,a.includes('+919250341382'))
              filterContacts.push(item)
          }
          })
        })
      }      
    })
    this.setState({filterContactList:filterContacts, availableList: filterContacts})
 //  console.log("iterate contact list",this.state.filterContactList)
  }

  componentDidMount() {
    this.getContactPermission();
    this.checkContactsAvailable();
  }

  checkContactsAvailable(){
    axios.get("allMobiles").then(res=>{
      this.setState({mobileNumberList:res.data})
      this.filterontactList();
    // console.log("mobile numbers",res.data)
    }).catch(err=>{

    })
  }


  searchContacts(search){
    let a = this.state.contactList.filter(item=>item.name.toLowerCase().includes(search))
   // console.log("search contact by",search,a)
  }

  onChangeSearch = (query) => {
   //   console.log('tests', query)
   this.setState({searchQuery:query}); 
   var availabeList = this.state.availableList.filter(item=>item.name.toLowerCase().includes(query))
   this.setState({availableList:availabeList})
   console.log('flter Search Contact')
    if(query === ''){
      this.resetList()
    }
   //var searchList  = this.state.filterContactList
  }

  resetList() {
    this.setState({availableList:this.state.filterContactList})
    
  }   
  goToChatScreen =(contact) =>{
    let a = contact.phoneNumbers[0].number.toString();
    a =  a.replace(/[()-]/g,'');
    a =  a.replace(/ /g,'');
    axios.get("getUserDetailsByMobileNumber", {params: {mobile: a }}).then(res => {
      console.log('mobile', res)
      this.props.navigation.navigate('UserChats', {userInfo : contact.phoneNumbers[0].number})
      console.log('123456', contact.phoneNumbers[0].number)
    }).catch(err => {

    })

    
  }

  render() {
    var temp = this.state.contactList;
    const contctList = this.state.availableList.map((data, index) => {
      return (
        <TouchableOpacity key={index} onPress={() => {this.goToChatScreen(data)}}>
        <View style={styles.notificationBox} key={index}>
          <Image style={styles.image} source={{uri: 'https://bootdey.com/img/Content/avatar/avatar3.png'}}></Image>
          <Text style={styles.name} >{data.name}</Text>
          </View>
          </TouchableOpacity>  
      )
    })
    return (
      <SafeAreaView style={{flex:1, backgroundColor:'#fff'}}>
        <View style={{backgroundColor:'#fff'}}>
          {/* <TextInput  onChangeText={(search) => this.searchContacts(search)} style={styles.searchBox} autoFocus={true} placeholder={'search'} placeholderTextColor={'#000'}></TextInput> */}
          <Searchbar
          placeholder="Search"
          onChangeText={query => this.onChangeSearch(query)}
          value={this.state.searchQuery}
      />
        </View>
        <ScrollView>
           {contctList}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  image:{
    width:45,
    height:45,
    borderRadius:20,
    marginLeft:20
  },
  name:{
    fontSize:15,
    fontWeight: 'bold',
    color: "#000000",
    marginLeft:10,
    alignSelf: 'center'
  },
  notificationBox: {
    paddingTop:10,
    paddingBottom:10,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  searchBox:{
    paddingTop:10,
    paddingBottom:10,
    backgroundColor: '#D3D3D3',
    borderRadius:20,
    borderWidth:1,
    borderColor:'#D3D3D3',
    paddingLeft:20
  }
});
