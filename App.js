import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();
import Chat from './component/Chat'
import Login from './component/login'
import Chats from './component/Chats'
import Contacts from './component/Contacts'
import Status from './component/Status'
import Call from './component/Call'

import OTP from './component/OTP'
import MainChatScreen from './component/MainChats'


export default function App() {


  function ChatStackScreen() {
    return (
      <Tab.Navigator>
        <Tab.Screen
          name='Chats'
          component={Chats}
          options={{ tabBarLabel: 'Chats' }}
        />
        <Tab.Screen
          name='Status'
          component={Status}
          options={{ tabBarLabel: 'Status' }}
        />
        <Tab.Screen
          name='Call'
          component={Call}
          options={{ tabBarLabel: 'Call' }}
        />
        {/* <Tab.Screen
          name="UserChats"
          component={Chat}
          options={{ tabBarLabel: 'Chat' }}
        /> */}

      </Tab.Navigator>
    );
  }

  return (
    <View style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="OTP" component={OTP} />
          <Stack.Screen name="Chat" component={ChatStackScreen} />
          <Stack.Screen name="Contacts" component={Contacts} />
          <Stack.Screen name="UserChats" component={Chat} />
        </Stack.Navigator>
      </NavigationContainer>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
});
